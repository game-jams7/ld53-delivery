using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Chat
{
    public class ChatApplication : MonoBehaviour
    {
        [SerializeField] Transform contactChatContainer;
        [SerializeField] Transform contactList;

        [SerializeField] ContactChat contactChatPrefab;
        [SerializeField] ContactItem contactItemPrefab;

        [SerializeField] Contact[] initialContacts;

        List<ContactChat> activeChats = new List<ContactChat>();
        List<ContactItem> activeContacts = new List<ContactItem>();

        void Start()
        {
            foreach (var c in initialContacts)
                LoadNewContact(c);

            SwitchChat(initialContacts.First().Uid);
        }

        void LoadNewContact(Contact contact)
        {
            var contactItem = Instantiate(contactItemPrefab, contactList);
            contactItem.Initialize(contact, SwitchChat);
            activeContacts.Add(contactItem);

            var contactChat = Instantiate(contactChatPrefab, contactChatContainer);
            contactChat.Initialize(contact);
            activeChats.Add(contactChat);
        }

        void SwitchChat(string contactId)
        {
            ResetStates();
            activeChats.First(c => c.ContactId.Equals(contactId)).Show();
            activeContacts.First(c => c.ContactId.Equals(contactId)).ShowPressed();
        }

        void ResetStates()
        {
            foreach (var c in activeChats)
                c.Hide();
            foreach (var c in activeContacts)
                c.ShowDepressed();
        }
    }
}
