﻿using System;

namespace Assets.Scripts.Chat
{
    [Serializable]
    public class ChatMessage
    {
        public string Text;
        public Sender Sender;

        public ChatMessage(Sender sender, string text)
        {
            Text = text;
            Sender = sender;
        }
    }
}