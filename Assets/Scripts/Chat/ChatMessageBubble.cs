﻿using TMPro;
using UnityEngine;

namespace Assets.Scripts.Chat
{
    public class ChatMessageBubble : MonoBehaviour
    {
        [SerializeField] TMP_Text text;

        public void Initialize(string message) =>
            text.text = message;
    }
}
