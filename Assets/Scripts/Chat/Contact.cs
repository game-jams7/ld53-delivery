using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Chat
{
    [CreateAssetMenu(fileName = "Contact", menuName = "Data/Contact")]
    public class Contact : ScriptableObject
    {
        public string FullName;
        public string Uid;
        public Sprite ProfilePic;
        public ChatMessage[] InitialMessages;
    }
}