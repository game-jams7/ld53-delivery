﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Chat
{
    public class ContactChat : MonoBehaviour
    {
        [SerializeField] ChatMessageBubble incomingPrefab;
        [SerializeField] ChatMessageBubble outgoingPrefab;
        [SerializeField] ScrollRect scroller;
        [SerializeField] Transform container;
        [SerializeField] TMP_Text nameLabel;
        [SerializeField] Image profilePic;
        [SerializeField] float timeBetweenMessages;
        [SerializeField] Button profileInfoButton;
        [SerializeField] ProfileInfoPopUp profileInfoPopUp;

        public string ContactId { get; private set; }

        public void Initialize(Contact contact)
        {
            ContactId = contact.Uid;
            nameLabel.text = contact.FullName;
            profilePic.sprite = contact.ProfilePic;

            profileInfoPopUp.SetProfilePhoto(contact.ProfilePic);
            profileInfoButton.onClick.AddListener(() => profileInfoPopUp.OpenProfileInfoPopUp());
            profileInfoPopUp.gameObject.SetActive(false);

            ShowMessages(contact.InitialMessages);
        }

        void ShowMessageThread(IEnumerable<ChatMessage> messages) 
        {
            for (int i = 0; i < messages.Count(); i++)
                StartCoroutine(WaitAndDeliverMessage(messages.ToArray()[i], i + 1 * timeBetweenMessages));
        }

        void ShowMessages(IEnumerable<ChatMessage> messages)
        {
            foreach (var m in messages)
            {
                if (m.Sender != Sender.Remote)
                    ShowOutgoing(m.Text);
                else
                    ShowIncoming(m.Text);
            }
        }

        IEnumerator WaitAndDeliverMessage(ChatMessage message, float time)
        {
            yield return new WaitForSeconds(time);
            if (message.Sender != Sender.Remote)
                ShowOutgoing(message.Text);
            else
                ShowIncoming(message.Text);
        }

        void ShowIncoming(string body)
        {
            var message = Instantiate(incomingPrefab, container);
            message.Initialize(body);
            Invoke("ScrollToBottom", 0.1f);
        }

        void ShowOutgoing(string body)
        {
            var message = Instantiate(outgoingPrefab, container);
            message.Initialize(body);
            Invoke("ScrollToBottom", 0.1f);
        }

        void ScrollToBottom() => scroller.normalizedPosition = Vector2.zero;

        public void Hide()
        {
            profileInfoPopUp.CloseProfileInfoPopUp();
            gameObject.SetActive(false);
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }
    }
}
