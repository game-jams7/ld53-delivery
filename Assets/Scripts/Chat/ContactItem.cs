using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Chat
{
    public class ContactItem : MonoBehaviour
    {
        [SerializeField] TMP_Text nameLabel;
        [SerializeField] Button button;
        [SerializeField] Image profilePhoto;

        [SerializeField] Color normalTint;
        [SerializeField] Color pressedTint;

        public string ContactId { get; private set; }
        public void Initialize(Contact contact, Action<string> onClick)
        {
            this.ContactId = contact.Uid;
            nameLabel.text = contact.FullName;
            profilePhoto.sprite = contact.ProfilePic;
            button.onClick.AddListener(() => onClick(contact.Uid));
        }

        public void ShowDepressed() => button.targetGraphic.color = normalTint;
        public void ShowPressed() => button.targetGraphic.color = pressedTint;
    }
}