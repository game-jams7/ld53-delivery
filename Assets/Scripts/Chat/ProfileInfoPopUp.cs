using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfileInfoPopUp : MonoBehaviour
{
    [SerializeField] private Button panelButton;
    [SerializeField] private Image profilePhoto;

    [SerializeField] private Animator animator;
    [SerializeField] private string closeTrigger = "close";

    private void Awake()
    {
        panelButton.onClick.AddListener(() => CloseProfileInfoPopUp());
    }

    public void OpenProfileInfoPopUp()
    {
        gameObject.SetActive(true);
    }

    public void SetProfilePhoto(Sprite photo)
    {
        profilePhoto.sprite = photo;
    }

    public void CloseProfileInfoPopUp()
    {
        animator.SetTrigger(closeTrigger);
        Invoke(nameof(Close), 0.2f);
    }

    private void Close()
    {
        gameObject.SetActive(false);
    }
}
