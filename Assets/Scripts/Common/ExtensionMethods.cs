﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Common
{
    public static class ExtensionMethods
    {
        public static T PickOne<T>(this IEnumerable<T> source) => 
            source.ToArray()[Random.Range(0, source.Count())];
        public static T PickOneExcept<T>(this IEnumerable<T> source, IEnumerable<T> exceptions)
        {
            var whiteList = source.Except(exceptions);
            return whiteList.ToArray()[Random.Range(0, whiteList.Count())];
        }
    }
}
