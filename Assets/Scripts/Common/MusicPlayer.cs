﻿using UnityEngine;

namespace Assets.Scripts.Common
{
    public class MusicPlayer : MonoBehaviour
    {
        void Awake()
        {
            var musicPlayers = FindObjectsOfType<MusicPlayer>();

            if (musicPlayers.Length > 1)
            {
                Destroy(this.gameObject);
            }

            DontDestroyOnLoad(this.gameObject);
        }
    }
}
