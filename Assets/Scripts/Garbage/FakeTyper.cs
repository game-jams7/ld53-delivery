using System;
using System.Text;
using TMPro;
using UnityEngine;

public class FakeTyper : MonoBehaviour
{
    [SerializeField] TMP_Text emailBody;

    string email = "Subject: Request for Release of [Name of Individual] from Police Custody\n\n" +
                   "Dear Police Commissioner [Last Name],\n\n" +
                   "I am writing to respectfully request your assistance in the matter of [Name of Individual], who is currently being held in police custody. I understand that this individual was arrested [insert time frame or date] and is currently awaiting trial on charges of [insert charges].\n\n" +
                   "As a Congressman representing [insert district or area], I have received numerous messages and calls from constituents expressing concern for the welfare of [Name of Individual]. I have also reviewed the case details and believe that there may be mitigating circumstances that warrant the release of this individual from custody pending trial.\n\n" +
                   "I would like to request that you consider the possibility of releasing [Name of Individual] from police custody under certain conditions, such as the provision of bail or house arrest. I understand that this decision lies within your discretion and that you must consider the safety of the community and the individual in question.\n\n" +
                   "However, I believe that in this case, [Name of Individual] has shown good faith and has cooperated fully with law enforcement officials. I am also aware of the significant impact that pre-trial detention can have on an individual's mental health, family, and employment.\n\n" +
                   "I respectfully request that you review the circumstances of [Name of Individual]'s case and consider my request for their release. I appreciate your attention to this matter and look forward to your response.\n\n" +
                   "Sincerely,\n\n" +
                   "[Your Name]\n" +
                   "Congressman, [District or Area]";

    StringBuilder writtenSoFar = new StringBuilder();
    int charactersPerKey = 3;
    int readIndex = 0;
    
    void Start()
    {
         
    }

    void Update()
    {
        if (Input.anyKeyDown)
        {
                var textToAppend = email[new Range(readIndex, readIndex + charactersPerKey)];
                writtenSoFar.Append(textToAppend);
                emailBody.text = writtenSoFar.ToString();

                readIndex += charactersPerKey;
        }
    }
}
