using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Fade : MonoBehaviour
{
    [SerializeField] Image fadeImage;

    public Action FadeInAction = () => { };
    public Action FadeOutAction = () => { };

    private void Awake()
    {
        StartCoroutine(FadeInTransition());
    }

    public IEnumerator FadeOutTransition()
    {
        for (float f = 0f; f <= 1; f += Time.deltaTime)
        {
            Color c = fadeImage.color;
            c.a = f;
            fadeImage.color = c;
            yield return null;
        }

        yield return new WaitForSeconds(0.2f);
        FadeOutAction?.Invoke();
    }

    public IEnumerator FadeInTransition()
    {
        for (float f = 1f; f >= 0; f -= Time.deltaTime)
        {
            Color c = fadeImage.color;
            c.a = f;
            fadeImage.color = c;
            yield return null;
        }

        yield return new WaitForSeconds(0.2f);
        FadeInAction?.Invoke();
    }
}
