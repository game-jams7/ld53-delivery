using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.MainMenu
{
    public class MainMenu : MonoBehaviour
    {
        [SerializeField] Button newGame;
        [SerializeField] Button exitGame;
        [SerializeField] Fade fade;

        void Start()
        {
            newGame.onClick.AddListener(NewGame);
            exitGame.onClick.AddListener(ExitGame);
        }

        void NewGame()
        {
            fade.FadeOutAction += LoadGame;
            StartCoroutine(fade.FadeOutTransition());
        }

        private void OnDisable()
        {
            fade.FadeOutAction -= LoadGame;
        }

        void LoadGame() => SceneManager.LoadScene("Desktop");
        void ExitGame() => Application.Quit();
    }
}
