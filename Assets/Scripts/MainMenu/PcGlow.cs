using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MainMenu
{
    public class PcGlow : MonoBehaviour
    {
        [SerializeField] Image image;
        [SerializeField] float frequency;

        void Start() => 
            StartCoroutine(Flicker());

        IEnumerator Flicker()
        {
            var color = image.color;
            color.a = Random.Range(0f,0.2f);
            frequency = Random.Range(0.5f,2f);
            image.color = color;

            yield return new WaitForSeconds(frequency);
            StartCoroutine(Flicker());
        }
    }
}
