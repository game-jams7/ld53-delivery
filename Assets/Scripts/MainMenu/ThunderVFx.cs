using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Common;
using UnityEngine;

public class ThunderVFx : MonoBehaviour
{
    [SerializeField] float intervalMin;
    [SerializeField] float intervalMax;
    [SerializeField] Animator animator;
    float thunderInterval;
    string[] triggers = {"thunder1", "thunder2", "thunder3"};

    void Start()
    {
        thunderInterval = Random.Range(intervalMin, intervalMax);
        StartCoroutine(WaitAndThunder());
    }

    IEnumerator WaitAndThunder()
    {
        thunderInterval = Random.Range(intervalMin, intervalMax);
        animator.SetTrigger(triggers.PickOne());

        yield return new WaitForSeconds(thunderInterval);
        StartCoroutine(WaitAndThunder());
    }
}
