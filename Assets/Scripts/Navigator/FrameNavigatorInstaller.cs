using System.Collections.Generic;
using UnityEngine;

namespace NavigatorTool
{
    public class FrameNavigatorInstaller : MonoBehaviour
    {
        [SerializeField] private List<FramePath> framePaths;
        [SerializeField] private string firstScreenId;

        private void Awake()
        {
            InitializeSimpleNavigator();
            NavigateToFirstScreen();
        }

        private void InitializeSimpleNavigator()
        {
            FrameNavigatorProvider.FrameNavigator.InitializeFrames(framePaths);
        }

        private void NavigateToFirstScreen()
        {
           FrameNavigatorProvider.FrameNavigator.OpenFrameById(firstScreenId);
        }
    }
}

