using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NavigatorTool
{
    public static class FrameNavigatorProvider
    {
        private static FrameNavigator _frameNavigator;

        public static FrameNavigator FrameNavigator=> _frameNavigator ??= new FrameNavigator();
    }
}