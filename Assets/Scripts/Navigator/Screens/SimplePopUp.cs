using UnityEngine;
using NavigatorTool;

public class SimplePopUp : MonoBehaviour, IPopUp
{
    [SerializeField] private CloseButton closeButton;

    public void Initialize()
    {
        closeButton.button.onClick.AddListener(() => FrameNavigatorProvider.FrameNavigator.CloseFrameById(closeButton.frameIdToClose));
    }

    public void Show()
    {
       gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
