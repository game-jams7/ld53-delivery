using System.Collections.Generic;
using UnityEngine;
using NavigatorTool;

public class SimpleScreen : MonoBehaviour, IScreen
{
    [Header("Buttons")]
    [SerializeField] private List<NavigationButton> navigationButtons;

    [Header("Components")]
    [SerializeField] private Animator animator;

    [Header("Properties")]
    [SerializeField] private string closeTrigger;

    public void Initialize()
    {
        foreach (var navigationButton in navigationButtons)
        {
            navigationButton.button.onClick.AddListener(() => FrameNavigatorProvider.FrameNavigator.OpenFrameById(navigationButton.toScreenId));
        }
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        animator?.SetTrigger(closeTrigger);

        Invoke(nameof(Disabled), 0.2f);
    }

    private void Disabled()
    {
        gameObject.SetActive(false);
    }
}