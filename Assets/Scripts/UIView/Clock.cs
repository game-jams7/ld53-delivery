using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UIView
{
    public class Clock : MonoBehaviour
    {
        [SerializeField] TMP_Text text;

        DateTime inGameTime;

        void Start()
        {
            inGameTime = new DateTime(2023, 4, 28, 20, 21, 0);
            UpdateTime();
            StartCoroutine(Tick());
        }

        IEnumerator Tick()
        {
            yield return new WaitForSeconds(1);
            inGameTime = inGameTime.AddSeconds(10);
            UpdateTime();
            StartCoroutine(Tick());
        }

        void UpdateTime() =>
            text.text = inGameTime.ToString("hh:mm tt");
    }
}
